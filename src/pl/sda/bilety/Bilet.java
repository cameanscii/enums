package pl.sda.bilety;

public enum Bilet {
    ULGOWY_GODZINNY(1.6, 60),
    ULGOWY_CALODNIOWY(5.20, 24 * 60),
    NORMALNY_GODZINNY(3.20, 60),
    NORMALNY_CALODNIOWY(10.40, 24 * 60),
    BRAK_BILETU(0, 0);


    private double cena;
    private int czasWMinutach;

    Bilet(double cena, int czasWMinutach) {
        this.cena = cena;
        this.czasWMinutach = czasWMinutach;
    }

    public double pobierzCeneBiletu() {
        return cena;
    }

    public double pobierzCzasJazdy() {
        return czasWMinutach;
    }

    public void wyswietlDaneOBilecie() {

        String bilet = this.toString().toLowerCase().split("_")[0];
        System.out.println(String.format("Bilet %s %d-godzinny", bilet, czasWMinutach / 60));
    }

    public static Bilet kupBilet(int wiek, double cena, int czasWMinutach) {
        Bilet bilet;

        if (wiek <= 18 || wiek >= 60) {
            if (czasWMinutach >= 60) {
                bilet = Bilet.ULGOWY_CALODNIOWY;
            } else {
                bilet = Bilet.ULGOWY_GODZINNY;
            }
        } else {
            if (czasWMinutach >= 60) {
                bilet = Bilet.NORMALNY_CALODNIOWY;
            } else {
                bilet = Bilet.NORMALNY_GODZINNY;
            }

        }

        if (bilet.pobierzCeneBiletu() <= cena) {
            return bilet;
        }

        return Bilet.BRAK_BILETU;

    }

    public void setCzasWMinutach(int czasWMinutach) {
        this.czasWMinutach = czasWMinutach;
    }
}
