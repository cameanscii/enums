package pl.sda.bilety;

public class Main {
    public static void main(String[] args) {
        Bilet bilet1 =Bilet.ULGOWY_GODZINNY;
        Bilet bilet2 = Bilet.NORMALNY_CALODNIOWY;
        Bilet bilet3 = Bilet.BRAK_BILETU;

        bilet1.wyswietlDaneOBilecie();
        bilet2.wyswietlDaneOBilecie();
        bilet3.wyswietlDaneOBilecie();

        //instancja klasy
       // Bilet bilet4 = Bilet.kupBilet(66,1,6);
        //bilet4.wyswietlDaneOBilecie();


        //bez instancji klasy, kozystamy w tym momencie ze metoda jest static
        Bilet.kupBilet(76,44,44).wyswietlDaneOBilecie();

    }

}
