package pl.sda.enumy;

public class Osoba {
    String imie;
    Plec plec;

    public Osoba(String imie, Plec plec) {
        this.imie = imie;
        this.plec = plec;
    }

    public void wyswietl(){
        System.out.printf("%s %s",imie,plec.getSymbol());
    }

    public static void main(String[] args) {
        Osoba ania = new Osoba("Ania",Plec.KOBIETA);
        ania.wyswietl();

    }
}
